// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'jquery'
import Vue from 'vue'
import App from './App'
import router from './router'
import Icon from 'vue-svg-icon/Icon.vue'
import VueScroller from 'vue-scroller'
import VueLazyload from 'vue-lazyload'
import VueTouch from 'vue-touch'
import VueAwesomeSwiper, { swiper, swiperSlide } from 'vue-awesome-swiper'

import Player from '@/components/player'

Vue.config.productionTip = false

// svg icon
Vue.component('icon', Icon)
//底端播放器组件
Vue.component('player',Player)
// 上拉&下拉组件
Vue.use(VueScroller)
// 移动端点击事件
Vue.use(VueTouch, {name: 'v-touch'})
// slider
Vue.use(VueAwesomeSwiper)
// 懒加载
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'http://oy9i8fcl4.bkt.clouddn.com/error.png',
  loading: 'http://oy9i8fcl4.bkt.clouddn.com/img-loading.svg',
  attempt: 1
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: {
    App,
    swiper,
    swiperSlide
  },
  template: '<App/>'
})
